﻿using Application.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Responses
{
    public class EditItemResponse
    {
        public int ListId { get; set; }

        public ItemDto Item { get; set; }
    }
}
