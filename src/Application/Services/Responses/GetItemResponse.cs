﻿using Application.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Responses
{
    public class GetItemResponse
    {
        public ItemDto Item { get; set; }
    }
}
