﻿using Application.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Responses
{
    public class GetItemsResponse
    {
        public int ListId { get; set; }

        public List<ItemDto> Items { get; set; }
    }
}
