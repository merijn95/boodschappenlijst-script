﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Requests
{
    public class GetItemRequest
    {
        public int ListId { get; set; }

        public int ItemId { get; set; }
    }
}
