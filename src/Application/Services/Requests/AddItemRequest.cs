﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Requests
{
    public class AddItemRequest
    {
        public int ListId { get; set; }

        public string ItemName { get; set; }
    }
}
