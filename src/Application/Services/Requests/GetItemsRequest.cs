﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Requests
{
    public class GetItemsRequest
    {
        public int ListId { get; set; }
    }
}
