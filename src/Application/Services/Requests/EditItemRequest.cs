﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Requests
{
    public class EditItemRequest
    {
        public int ListId { get; set; }

        public int ItemId { get; set; }

        public string NewItemName { get; set; }
    }
}
