﻿using Application.Entities;
using Application.Exceptions;
using Application.Repositories.Interfaces;
using Application.Services.Interfaces;
using Application.Services.Requests;
using Application.Services.Responses;
using Application.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Application.Services
{
    public sealed class ListService : IListService
    {
        private readonly IAsyncRepository<ItemList> _listRepository;

        public ListService(IAsyncRepository<ItemList> listRepository)
        {
            _listRepository = listRepository;
        }

        /// <summary>
        /// Get the item list from the repository and then get an item from that item list.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<GetItemResponse> GetItemAsync(GetItemRequest request)
        {
            var itemList = await GetItemListAsync(listId: request.ListId);
            var item = itemList.GetItemById(id: request.ItemId);

            return new GetItemResponse
            {
                Item = new ItemDto
                {
                    Id = item.Id,
                    Name = item.Name
                }
            };
        }

        /// <summary>
        /// Get items from the repository.
        /// </summary>
        /// <param name="request"></param>
        /// <exception cref="EmptyItemsException">Will be thrown if there are no items in the list.</exception>
        /// <returns></returns>
        public async Task<GetItemsResponse> GetItemsAsync(GetItemsRequest request)
        {
            var itemList = await GetItemListAsync(listId: request.ListId);
            var items = itemList.GetItems();

            var response = new GetItemsResponse
            {
                ListId = request.ListId,
                Items = new List<ItemDto>()
            };

            foreach (var item in items)
            {
                response.Items.Add(new ItemDto
                {
                    Id = item.Id,
                    Name = item.Name
                });
            }

            return response;
        }

        /// <summary>
        /// Add an item to the repository.
        /// </summary>
        /// <param name="request">Request data</param>
        /// <exception cref="ListNotFoundException">Will be thrown if request.ListId is not found</exception>
        /// <returns>Response data</returns>
        public async Task<AddItemResponse> AddItemAsync(AddItemRequest request)
        {
            var itemList = await GetItemListAsync(listId: request.ListId);
            var item = new Item(name: request.ItemName);

            itemList.AddItem(item);

            await _listRepository.UpdateAsync(itemList);

            return new AddItemResponse
            {
                ListId = request.ListId,
                Item = new ItemDto
                {
                    Id = item.Id,
                    Name = item.Name
                }
            };
        }

        /// <summary>
        /// Edit an item.
        /// </summary>
        /// <param name="request">Possible request data</param>
        /// <returns></returns>
        public async Task<EditItemResponse> EditItemAsync(EditItemRequest request)
        {
            var itemList = await GetItemListAsync(listId: request.ListId);
            var item = itemList.GetItemById(id: request.ItemId);

            item.SetName(request.NewItemName);
            itemList.EditItem(item);

            await _listRepository.UpdateAsync(itemList);

            return new EditItemResponse
            {
                ListId = request.ListId,
                Item = new ItemDto
                {
                    Id = item.Id,
                    Name = request.NewItemName
                }
            };
        }

        /// <summary>
        /// Remove an item from the item list and persist the changes to the repository.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task RemoveItemAsync(RemoveItemRequest request)
        {
            var itemList = await GetItemListAsync(listId: request.ListId);
            itemList.RemoveItemById(id: request.ItemId);

            await _listRepository.UpdateAsync(itemList);
        }

        /// <summary>
        /// Helper function that retrieves the item list.
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        private async Task<ItemList> GetItemListAsync(int listId)
        {
            var list = await _listRepository.GetAsync(id: listId);
            if (list == null)
            {
                throw new ListNotFoundException($"List with id {listId} not found");
            }

            return list;
        }
    }
}
