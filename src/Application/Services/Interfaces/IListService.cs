﻿using Application.Entities;
using Application.Services.Requests;
using Application.Services.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IListService
    {
        Task<GetItemResponse> GetItemAsync(GetItemRequest request);

        Task<GetItemsResponse> GetItemsAsync(GetItemsRequest request);

        Task<AddItemResponse> AddItemAsync(AddItemRequest request);

        Task<EditItemResponse> EditItemAsync(EditItemRequest request);

        Task RemoveItemAsync(RemoveItemRequest request);
    }
}
