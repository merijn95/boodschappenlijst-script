﻿using Application.Entities;
using Application.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories.Interfaces
{
    public interface IAsyncRepository<T> where T : IAggregateRoot
    {
        Task<T> GetAsync(int id);

        Task UpdateAsync(T entity);
    }
}
