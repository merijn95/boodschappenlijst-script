﻿using Ardalis.GuardClauses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Entities
{
    public class Item : BaseEntity
    {
        public string Name { get; private set; }

        public Item(string name)
        {
            SetName(name);
        }

        public void SetName(string name)
        {
            Guard.Against.NullOrWhiteSpace(name, nameof(name));
            Name = name;
        }
    }
}
