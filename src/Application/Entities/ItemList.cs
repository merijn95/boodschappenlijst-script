﻿using Application.Entities.Interfaces;
using Application.Exceptions;
using Ardalis.GuardClauses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Entities
{
    public class ItemList : BaseEntity, IAggregateRoot
    {
        private readonly List<Item> _items = new List<Item>();

        public IReadOnlyList<Item> Items => _items.AsReadOnly();

        /// <summary>
        /// Get all items.
        /// </summary>
        /// <exception cref="EmptyItemsException">Will be thrown if there are no items in the list.</exception>
        /// <returns></returns>
        public IReadOnlyList<Item> GetItems()
        {
            if (_items.Count == 0)
            {
                throw new EmptyItemsException($"No items found in list with id {Id}");
            }

            return Items;
        }

        /// <summary>
        /// Get an item from the list.
        /// </summary>
        /// <param name="id">Unique item identifier.</param>
        /// <exception cref="ItemDoesNotExistException">Will be thrown if an item does not exist.</exception>
        /// <returns></returns>
        public Item GetItemById(int id)
        {
            var result = _items.FirstOrDefault(c => c.Id == id);
            if (result == null)
            {
                throw new ItemDoesNotExistException($"Item with id {id} not found");
            }

            return result;
        }

        /// <summary>
        /// Add a new item to the list.
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(Item item)
        {
            Guard.Against.NullOrWhiteSpace(item.Name, nameof(item.Name));

            _items.Add(item);
        }

        /// <summary>
        /// Edit an item.
        /// </summary>
        /// <exception cref="ItemDoesNotExistException">Will be thrown if an item hasn't been found.</exception>
        /// <param name="item">Item that'll be modified.</param>
        public void EditItem(Item item)
        {
            Guard.Against.NullOrWhiteSpace(item.Name, nameof(item.Name));

            var result = _items.FirstOrDefault(c => c.Id == item.Id);
            if (result == null)
            {
                throw new ItemDoesNotExistException($"Item with id {item.Id} not found");
            }
            
            var index = _items.IndexOf(result);
            _items[index] = item;
        }

        /// <summary>
        /// Remove an item from the list.
        /// </summary>
        /// <exception cref="ItemDoesNotExistException">Will be thrown if the specified item does not exist.</exception>
        /// <param name="id">Item id</param>
        public void RemoveItemById(int id)
        {
            Guard.Against.NegativeOrZero(id, nameof(id));

            var result = _items.FirstOrDefault(c => c.Id == id);
            if (result == null)
            {
                throw new ItemDoesNotExistException($"Item with id {id} not found");
            }

            _items.Remove(result);
        }
    }
}
