﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Exceptions
{
    public class EmptyItemsException : Exception
    {
        public EmptyItemsException(string message) : base(message)
        {

        }
    }
}
