﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Exceptions
{
    public class ItemDoesNotExistException : Exception
    {
        public ItemDoesNotExistException(string message) : base(message)
        {

        }
    }
}
