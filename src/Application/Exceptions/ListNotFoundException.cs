﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Exceptions
{
    public class ListNotFoundException : Exception
    {
        public ListNotFoundException(string message) : base(message)
        {

        }
    }
}
