﻿using Application.Entities;
using Application.Exceptions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Tests.Entities
{
    class ItemListTest
    {
        [Test]
        public void GetItems()
        {
            var itemList = new ItemList();
            itemList.AddItem(new Item("Test") { Id = 1 });

            var items = itemList.GetItems();
            Assert.AreEqual(1, items.Count);
            Assert.AreEqual(1, items[0].Id);
            Assert.AreEqual("Test", items[0].Name);
        }

        [Test]
        public void GetItems_ThrowsEmptyItemsException()
        {
            var itemList = new ItemList();

            Assert.Throws<EmptyItemsException>(() => itemList.GetItems());
        }

        [Test]
        public void GetItemById()
        {
            var itemList = new ItemList();
            itemList.AddItem(new Item("Test") { Id = 1 });

            var item = itemList.GetItemById(id: 1);
            Assert.AreEqual(1, item.Id);
            Assert.AreEqual("Test", item.Name);
        }

        [Test]
        public void GetItemById_ThrowsItemDoesNotExistException()
        {
            var itemList = new ItemList();

            Assert.Throws<ItemDoesNotExistException>(() => itemList.GetItemById(id: 1));
        }

        [Test]
        public void AddItem()
        {
            var expectedItem = new Item(name: "Test");

            var list = new ItemList();

            list.AddItem(expectedItem);

            Assert.AreEqual(1, list.Items.Count);
            Assert.AreEqual(expectedItem, list.Items[0]);
        }

        [Test]
        public void AddItem_ThrowsArgumentException_IfName_IsNullOrWhitespace()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                var list = new ItemList();
                list.AddItem(new Item(null));
;           });

            Assert.Throws<ArgumentException>(() =>
            {
                var list = new ItemList();
                list.AddItem(new Item(""));
            });
        }

        [Test]
        public void EditItem_ModifyName()
        {
            var list = new ItemList();
            var item = new Item("Test")
            {
                Id = 1,
            };

            list.AddItem(item);

            Assert.AreEqual("Test", list.Items[0].Name);

            var expected = "Blabla";
            item = new Item("Blabla")
            {
                Id = 1
            };

            list.EditItem(item);

            Assert.AreEqual(expected, list.Items[0].Name);
        }

        [Test]
        public void EditItem_ThrowItemDoesNotExistException_IfItemIsNotFound()
        {
            var items = new List<Item>
            {
                new Item("Test")
                {
                    Id = 1,
                },
                new Item("Test")
                {
                    Id = 2,
                }
            };

            var itemList = new ItemList();
            var prop = itemList.GetType().GetField("_items", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            prop.SetValue(itemList, items);

            // Sanity check
            Assert.AreEqual(items.Count, itemList.Items.Count);

            Assert.Throws<ItemDoesNotExistException>(() =>
            {
                var itemThatDoesNotExist = new Item("Test")
                {
                    Id = 1337
                };

                itemList.EditItem(itemThatDoesNotExist);
            });
        }

        [Test]
        public void RemoveItemById()
        {
            var fakeItemList = new ItemList();
            fakeItemList.AddItem(new Item("Test") { Id = 1 });
            fakeItemList.AddItem(new Item("Test") { Id = 2 });

            Assert.AreEqual(2, fakeItemList.Items.Count);

            fakeItemList.RemoveItemById(id: 1);

            Assert.AreEqual(1, fakeItemList.Items.Count);
            Assert.AreEqual(2, fakeItemList.Items[0].Id);
        }

        [Test]
        public void RemoveItemById_ThrowsArgumentException()
        {
            var fakeItemList = new ItemList();

            Assert.Throws<ArgumentException>(() => fakeItemList.RemoveItemById(0));
            Assert.Throws<ArgumentException>(() => fakeItemList.RemoveItemById(-1));
        }

        [Test]
        public void RemoveItemById_ThrowsItemDoesNotExistException()
        {
            var fakeItemList = new ItemList();

            Assert.Throws<ItemDoesNotExistException>(() => fakeItemList.RemoveItemById(1));
        }
    }
}
