﻿using Application.Dto;
using Application.Entities;
using Application.Exceptions;
using Application.Repositories.Interfaces;
using Application.Services;
using Application.Services.Requests;
using Application.Services.Responses;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tests.Services
{
    class ListServiceTests
    {
        private Mock<IAsyncRepository<ItemList>> _itemRepositoryMock;

        public ListServiceTests()
        {
            _itemRepositoryMock = new Mock<IAsyncRepository<ItemList>>();
        }

        [SetUp]
        public void Init()
        {
            _itemRepositoryMock.Reset();
        }

        [Test]
        public async Task GetItemAsync()
        {
            var fakeItemList = new ItemList() { Id = 1 };
            fakeItemList.AddItem(new Item("Test") { Id = 1 });

            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).ReturnsAsync(fakeItemList);

            var listService = new ListService(_itemRepositoryMock.Object);

            var response = await listService.GetItemAsync(new GetItemRequest { ItemId = 1, ListId = 1 });
            var expected = new GetItemResponse
            {
                Item = new ItemDto
                {
                    Id = 1,
                    Name = "Test"
                }
            };

            Assert.AreEqual(expected.Item.Id, response.Item.Id);
            Assert.AreEqual(expected.Item.Name, response.Item.Name);
        }

        [Test]
        public void GetItemAsync_ThrowListNotFoundException()
        {
            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).Returns(Task.FromResult<ItemList>(null));
            var listService = new ListService(_itemRepositoryMock.Object);

            Assert.ThrowsAsync<ListNotFoundException>(async () => await listService.GetItemAsync(new GetItemRequest()));

        }

        [Test]
        public void GetItemsAsync()
        {
            var fakeItemList = new ItemList() { Id = 1 };
            fakeItemList.AddItem(new Item("Test") { Id = 1 });
            fakeItemList.AddItem(new Item("Test") { Id = 2 });
            fakeItemList.AddItem(new Item("Test") { Id = 3 });

            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).ReturnsAsync(fakeItemList);

            var expected = new GetItemsResponse
            {
                ListId = 1,
                Items = new List<ItemDto>
                {
                   new ItemDto
                   {
                       Id = 1,
                       Name = "Test"
                   },
                   new ItemDto
                   {
                       Id = 2,
                       Name = "Test"
                   },
                   new ItemDto
                   {
                       Id = 3,
                       Name = "Test"
                   }
                }
            };
        }

        [Test]
        public void GetItemsAsync_ThrowListNotFoundException_IfListHasNotBeenFound()
        {
            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).Returns(Task.FromResult<ItemList>(null));
            var listService = new ListService(_itemRepositoryMock.Object);
            var request = new AddItemRequest
            {
                ListId = 1,
                ItemName = "Test"
            };

            Assert.ThrowsAsync<ListNotFoundException>(async () => await listService.AddItemAsync(request));
        }

        [Test]
        public async Task AddItemAsync()
        {
            var itemList = new ItemList();
            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).ReturnsAsync(itemList);
            _itemRepositoryMock.Setup(c => c.UpdateAsync(It.IsAny<ItemList>())).Returns(Task.CompletedTask);
            var listService = new ListService(_itemRepositoryMock.Object);

            var request = new AddItemRequest
            {
                ListId = 1,
                ItemName = "Test"
            };

            var expected = new AddItemResponse
            {
                ListId = 1,
                Item = new Dto.ItemDto
                {
                    Name = "Test"
                }
            };

            var response = await listService.AddItemAsync(request);

            Assert.AreEqual(expected.ListId, response.ListId);
            Assert.AreEqual(expected.Item.Name, response.Item.Name);
            Assert.IsTrue(itemList.Items.Count == 1);
        }

        [Test]
        public void AddItemAsync_ThrowListNotFoundException_IfListHasNotBeenFound()
        {
            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).Returns(Task.FromResult<ItemList>(null));
            var listService = new ListService(_itemRepositoryMock.Object);
            var request = new AddItemRequest
            {
                ListId = 1,
                ItemName = "Test"
            };

            Assert.ThrowsAsync<ListNotFoundException>(async () => await listService.AddItemAsync(request));
        }

        [Test]
        public async Task EditItemAsync()
        {
            var fakeItemList = new ItemList() { Id = 1 };
            fakeItemList.AddItem(new Item(name: "Test") { Id = 1 });

            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).ReturnsAsync(fakeItemList);
            _itemRepositoryMock.Setup(c => c.UpdateAsync(It.IsAny<ItemList>())).Returns(Task.CompletedTask);

            var listService = new ListService(_itemRepositoryMock.Object);

            var newItemName = "Bladiebla";
            var expected = new EditItemResponse
            {
                ListId = 1,
                Item = new Dto.ItemDto
                {
                    Id = 1,
                    Name = newItemName
                }
            };

            var response = await listService.EditItemAsync(new EditItemRequest
            {
                ListId = 1,
                ItemId = 1,
                NewItemName = newItemName
            });

            Assert.AreEqual(expected.ListId, response.ListId);
            Assert.AreEqual(expected.Item.Id, response.Item.Id);
            Assert.AreEqual(expected.Item.Name, response.Item.Name);
        }

        [Test]
        public void EditItemAsync_ThrowListNotFoundException_IfListHasNotBeenFound()
        {
            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).Returns(Task.FromResult<ItemList>(null));
            var listService = new ListService(_itemRepositoryMock.Object);
            var request = new AddItemRequest
            {
                ListId = 1,
                ItemName = "Test"
            };

            Assert.ThrowsAsync<ListNotFoundException>(async () => await listService.AddItemAsync(request));
        }

        [Test]
        public async Task RemoveItemAsync()
        {
            var fakeItemList = new ItemList() { Id = 1 };
            fakeItemList.AddItem(new Item("Test") { Id = 1 });
            fakeItemList.AddItem(new Item("Test") { Id = 2 });
            fakeItemList.AddItem(new Item("Test") { Id = 3 });

            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).ReturnsAsync(fakeItemList);
            var listService = new ListService(_itemRepositoryMock.Object);

            await listService.RemoveItemAsync(new RemoveItemRequest { ListId = 1, ItemId = 1 });

            Assert.AreEqual(2, fakeItemList.Items.Count);

            await listService.RemoveItemAsync(new RemoveItemRequest { ListId = 1, ItemId = 2 });

            Assert.AreEqual(1, fakeItemList.Items.Count);

            await listService.RemoveItemAsync(new RemoveItemRequest { ListId = 1, ItemId = 3 });

            Assert.AreEqual(0, fakeItemList.Items.Count);
        }

        [Test]
        public void RemoveItemAsync_ThrowsListNotFoundException()
        {
            _itemRepositoryMock.Setup(c => c.GetAsync(It.IsAny<int>())).Returns(Task.FromResult<ItemList>(null));
            var listService = new ListService(_itemRepositoryMock.Object);
           
            Assert.ThrowsAsync<ListNotFoundException>(async () => await listService.RemoveItemAsync(new RemoveItemRequest()));
        }
    }
}
